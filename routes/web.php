<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\UsuarioController;

$router->get('/', function () use ($router) {
    return $router->app->version();
});
//--------- - ruta de Tercero---------------------------------------------
$router->get('/tercero/rif/{rif}','TerceroController@getTerceroRif');
$router->post('/tercero/crear','TerceroController@crearTerceros');
$router->post('/tercero/modificar','TerceroController@modificarTerceros');
$router->post('/tercero/eliminar','TerceroController@eliminarTerceros');
//-----------rutas de direcciones -------------------------------------------
$router->get('/direccion/tercero/{tercero_id}','AddressController@getAddressTercero');
$router->post('/direccion/modificar','AddressController@modificarAddress');
$router->post('/direccion/eliminar','AddressController@eliminarAddress');

//-----------Rutas de Usuarios---------------------------------------------------
$router->get('/usuario/user/{user}', 'UsuarioController@getUsuario');
$router->post('/usuario/crear','UsuarioController@crearUsuario');
$router->post('/usuario/modificar','UsuarioController@crearModificar');
$router->post('/usuario/eliminar','UsuarioController@crearEliminar');

//-------------Rutas de Categoria----------------------------------------------------
$router->get('/categoria/{cate}', 'CategoriaController@getCategoria');
$router->post('/categoria/crear','CategoriaController@crearCategoria');
$router->post('/categoria/modificar','CategoriaController@modificarCategoria');
$router->post('/categoria/eliminar','CategoriaController@eliminarCategoria');
//-------------Rutas de Moneda----------------------------------------------------
$router->get('/moneda/{mone}', 'MonedaController@getMoneda');
$router->post('/moneda/crear','MonedaController@crearMoneda');
$router->post('/moneda/modificar','MonedaController@modificarMoneda');
$router->post('/moneda/eliminar','MonedaController@eliminarMoneda');
//-------------Rutas de Producto----------------------------------------------------
$router->get('/producto/{prod}', 'ProductoController@getProducto');
$router->post('/producto/crear','ProductoController@crearProducto');
$router->post('/producto/modificar','ProductoController@modificarProducto');
$router->post('/producto/eliminar','ProductoController@eliminarProducto');

//-------------Rutas de Image----------------------------------------------------
$router->get('/image/{ima}', 'ImageController@getImage');
$router->post('/image/crear','ImageController@crearImage');
$router->post('/image/modificar','ImageController@modificarImage');
$router->post('/image/eliminar','ImageController@eliminarImage');

//--------------Rutas de cierre----------------------------------------
$router->post('/cierre/z/crear','CierreController@crearCierreZ');
$router->post('/cierre/crear','CierreController@crearCierre');
$router->get('/cierre/buscar/{fecha}','CierreController@buscarCierre');
//---------------Ruta de cobro-------------------------------------------
$router->post('/cobro/crear','CobroController@crearCobro');
$router->get('/cobro/buscar/fecha/{fecha}','CobroController@buscarCobroXfecha');
$router->get('/cobro/buscar/tercero/{tercero_id}','CobroController@buscarCobroXtercero');

//---------------Ruta de convercion-------------------------------------------
$router->post('/conversion/crear','ConversionController@crearConversion');
$router->get('/conversion/buscar/fecha/{fecha}','ConversionController@buscarConversionXfecha');

//---------------Ruta de detalle-------------------------------------------
$router->post('/detalle/crear','DetalleController@crearDetalle');
$router->get('/detalle/buscar/fecha/{fecha}','DetalleController@buscarDetalleXfecha');
//---------------Ruta de Metodo-------------------------------------------
$router->post('/metodo/crear','MetodoController@crearMetodo');

//---------------Ruta de Pedido-------------------------------------------
$router->post('/pedido/crear','PedidoController@crearPedido');
$router->post('/pedido/modificar','PedidoController@modificarPedido');
$router->post('/pedido/eliminar','PedidoController@eliminarPedido');

//---------------Ruta de Pricelist-------------------------------------------
$router->post('/pricelist/crear','PricelistController@crearPricelist');
$router->post('/pricelist/modificar','PricelistController@modificarPricelist');
$router->post('/pricelist/eliminar','PricelistController@eliminarPricelist');

//---------------Ruta de Pricelistdetail-------------------------------------------
$router->post('/pricelistdetail/crear','PricelistdetailController@crearPricelistdetail');
$router->post('/pricelistdetail/modificar','PricelistdetailController@modificarPricelistdetail');
$router->post('/pricelistdetail/eliminar','PricelistdetailController@eliminarPricelistdetail');

//---------------Ruta de Sesion-------------------------------------------
$router->post('/sesion/crear','SesionController@crearSesion');
$router->post('/sesion/eliminar','SesionController@eliminarSesion');

//---------------Ruta de Terminal-------------------------------------------
$router->post('/Terminal/crear','TerminalController@crearTerminal');
$router->post('/Terminal/modificar','TerminalController@modificarTerminal');
$router->post('/Terminal/eliminar','TerminalController@eliminarTerminal');




