<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model {

    protected $fillable = ["name", "codigo","isrecipe","image_id","description","uom","product_id","qty","status","categoria_id"];

    protected $dates = [];
    public $timestamps = false;

    public static $rules = [
        'name' => 'required|max:100',
        'codigo' => 'required|max:60',
        'description' => 'max:255|size:255',
        'qty' => 'max:10|integer|size:10',
        'uom' => 'max:11|integer|size:11',
    ];

    public static $messages = [
        'name.required' => 'El Nombre es Requerido',
        'name.max' => 'El Maximo de caracter es de 100',
        'codigo.required' => 'El Codigo es Requerido',
        'dcodigo.max' => 'El Maximo de caracter es de 60',
        'description.max' => 'El Maximo de caracter es de 255',
        'qty.max' => 'El Maximo de Caracter es de 10',
        'qty.integer' => 'Solo acepta numeros',
        'uom.max' => 'El Maximo de Caracter es de 11',
        'uom.integer' => 'Solo acepta numeros',
    ];


  // funciones Relacion -------------------------------------------------------
  public function detalle()
        {
            return $this->hasMany('App\Detalle');
        }
        public function medida()
              {
                  return $this->hasMany('App\ProductoMedida');
              }

        public function image()
        {
            return $this->belongsTo('App\Image');
        }

        public function categoria()
        {
            return $this->belongsTo('App\Categoria');
        }
}
