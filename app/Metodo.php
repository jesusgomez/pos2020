<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Metodo extends Model {

    protected $fillable = ["name", "print","isdefault","status","orden","moneda_id"];

    protected $dates = [];
    public $timestamps = false;

    public static $rules = [
        'name' => 'required|max:30',
        'print' => 'required|max:12',
        'orden' => 'required'
    ];

    public static $messages = [
        'name.required' => 'El Nombre es Requerido',
        'name.max' => 'El Maximo de Caracter es de 30',
        'print.required' => 'Print es Requerido',
        'print.max' => 'El Maximo de Caracter es de 12',
        'orden.required' => 'El Numero de Orden  es Requerido',
    ];

//-----------------------------

            public function cobro()
            {
                return $this->hasMany('App\Cobro');
            }

            public function moneda()
            {
                return $this->belongsTo('App\Moneda');
            }
}
