<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {
    protected $table = 'producto_medida';
    protected $fillable = ["producto_id", "medida_id","cantidad","status"];

    protected $dates = [];
    public $timestamps = false;

    public static $rules = [
        'cantidad' => 'required',

    ];

    public static $messages = [
        'cantidad.required' => 'La cantidad  es Requerido',

    ];
   // funcion Relacciones ---------------------------------------------------------------------------------------

 public function producto()
    {
        return $this->belongsTo('App\Producto');
    }
public function medida()
   {
       return $this->belongsTo('App\Medida');
     }

}
