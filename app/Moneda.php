<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Moneda extends Model {

    protected $fillable = ["name", "symbol","status"];

    protected $dates = [];
    public $timestamps = false;

    public static $rules = [
        'name' => 'required|max:20',
        'symbol' => 'required|max:5',
    ];

    public static $messages = [
        'name.required' => 'El Nombre es Requerido',
        'name.max' => 'El Maximo de Caracter es de 20',
        'symbol.required' => 'Simbolo Requerido',
        'symbol.max' => 'El Maximo de Caracter es de 5',
        ];

     // funcion Relacciones ---------------------------------------------------------------------------------------

        public function conversion()
        {
            return $this->hasMany('App\Conversion');
        }

        public function metodo()
            {
                return $this->hasMany('App\Metodo');
            }

}
 