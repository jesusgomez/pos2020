<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricelistdetail extends Model {
    protected $table = 'pricelistdetails';
    protected $fillable = ["product_id", "price", "status","pricelist_id"];

    protected $dates = [];
    public $timestamps = false;




    //funciones Relaciones entre Tablas ----------------------------------------------------

    public function producto()
        {
            return $this->hasMany('App\Producto');
        }
      
        public function pricelist()
        {
            return $this->hasMany('App\Pricelist');
        }
      

}
