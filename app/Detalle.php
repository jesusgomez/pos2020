<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model {

    protected $fillable = ["pedido_id", "producto_id","name","qty","price","discount","tax","status"];

    protected $dates = [];
    

    public static $rules = [
        'name'=> 'required|max:100',
        'qty' => 'required|max:11',
        'price' => 'required',
        'discount' => 'required',
        'tax' => 'required',

    ];

    public static $messages = [
        'name.required' => 'El nombre es Requerido',
        'qty.required'  => 'La Cantidad es Requerida',
        'price.required'  => 'El Precio es Requerido',
        'discount.required'  => 'El Descuento es Requerido',
        'tax.required' => 'La tasa es Requerida',

    ];

    // funcion Relacciones ---------------------------------------------------------------------------------------

    public function pedido()
        {
            return $this->belongsTo('App\Pedido');
        }


        

        public function producto()
        {
            return $this->belongsTo('App\Producto');
        }

}
