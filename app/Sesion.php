<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Sesion extends Model {
    protected $table = 'sesiones';
    protected $fillable = ["usuario_id", "terminal_id", "status"];

    protected $dates = [];

    public static $rules = [

    ];

    public static $messages = [

    ];

    // -------------------
    public function cierre()
        {
            return $this->hasMany('App\Cierre');
        }

        public function pedido()
    {
        return $this->hasMany('App\Pedido');
    }

    public function usuario()
    {
        return $this-> belongsTo('App\Usuario');
    }

    public function terminal()
    {
        return $this->belongsTo('App\Terminal');
    }
}
