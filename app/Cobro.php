<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cobro extends Model {

    protected $fillable = ["tercero_id", "pedido_id","metodo_id","total","balance","status","created_by","updated_by"];

   // protected $dates = [];

    public static $rules = [

    ];

    public static $messages = [

    ];

 //-----------------------------

    public function cobroCierre()
    {
        return $this->hasMany('App\CierreCobro');
    }

    public function pedido()
        {
            return $this->belongsTo('App\Pedido');
        }
    public function metodo()
        {
           return $this->belongsTo('App\Metodo');
        }

    public function tercero()
        {
            return $this->belongsTo('App\Tercero');
        }

}
