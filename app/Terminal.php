<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminal extends Model {
    protected $table = 'terminales';
    protected $fillable = ["name", "ip", "mac","port","type","status","terminal_id"];

    protected $dates = [];
    public $timestamps = false;

    public static $rules = [
            'name' => 'required|max:20',
            'ip' => 'required|max:20',
            'mac' => 'required|max:20',
            'port' => 'required|max:5',
            'type' => 'in:POS,Comandera,Fiscal,Balanza,Punto|required',
    ];

    public static $messages = [
        'name.required' => 'El Nombre es Requerido',
        'name.max' => 'EL Maximo de Caracter es de 20',
        'ip.required' => 'El Nombre es Requerido',
        'ip.max' => 'EL Maximo de Caracter es de 20',
        'mac.required' => 'El Nombre es Requerido',
        'mac.max' => 'EL Maximo de Caracter es de 20',
        'port.required' => 'El Nombre es Requerido',
        'port.max' => 'EL Maximo de Caracter es de 5',
        'type.required' => 'El Nombre es Requerido',
        'type.in' => 'Solo es aceptado los siguientes: "POS,Comandera,Fiscal,Balanza,Punto" ',

    ];
    //funciones Relaciones entre Tablas ----------------------------------------------------

    public function cierre()
        {
            return $this->hasMany('App\Cierre');
        }
        public function sdesion()
        {
            return $this->hasMany('App\Sesion');
        }


}
