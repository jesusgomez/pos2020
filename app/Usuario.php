<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model {

    protected $fillable = ["name", "user", "pass","islogged","token_id","status"];

    protected $dates = [];

    public static $rules = [
            'name' => 'required|max:60',
            'user' => 'required|max:15|unique:posts,user',
            'pass' => 'required|max:64',
    ];

    public static $messages = [
        'name.required' => 'El Nombre es Requerido',
        'name.max' => 'El Maximo de Caracteres es de 60',
        'user.required' => 'El Usuario es Requerido',
        'user.unique' => 'El Usuario debe ser Unico, ya este se encuentra reguistrado en la base de datos',
        'user.max' => 'El Maximo de Caracteres es de 15',
        'pass.required' => 'La contraseña es Requerida',
        'pass.max' => 'El Maximo de Caracteres es de 64',
    ];


    // funciones Relacion ----------------------------------------------------------------

    public function sesion()
    {
        return $this->hasMany('App\Sesion');
    }
}
