<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model {

    protected $fillable = ["pedido_id", "sesion_id","tercero_id","total","discount","tax","date","type","docstatus", "issync", "status", "updated_by", "created_by"];

    protected $dates = [];

    public static $rules = [
        'total' => 'required',
        'discount' => 'required',
        'tax' => 'required',
        'date' => 'required',
        'type' => 'in:invoice,order,return',
        'docstatus' => 'in:draft,book',

    ];

    public static $messages = [
        'total.required' => 'El Tolal es Requerido',
        'discount.required' => 'Descuento es Requerido',
        'tax.required' => 'Tasa es Requerida',
        'date.required' => 'La Fecha es Requerida',
        'type.in' => 'Solo se debe poner "invoice, order o return"',
        'docstatus.in' => 'Solo se debe poner "draft o book"',
    ];

    //-------------------------------------------
    public function cobro()
    {
        return $this->hasMany('App\Cobro');
    }

    public function detalle()
    {
        return $this->hasMany('App\Detalle');
    }

    public function sesion()
    {
        return $this-> belongsTo('App\Sesion');
    }

    public function tercero()
    {
        return $this-> belongsTo('App\Tercero');
    }
}
