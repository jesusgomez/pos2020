<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class Conversion extends Model {
    protected $table = 'conversiones';
    protected $fillable = ["moneda_id", "factor", "status","updated_by","created_by"];

    protected $dates = [];

    public static $rules = [

    ];

    public static $messages = [

    ];

    // funcion Relacciones ---------------------------------------------------------------------------------------

    public function moneda()
        {
            return $this->belongsTo('App\Moneda');
        }

}
