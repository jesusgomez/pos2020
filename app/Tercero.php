<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tercero extends Model {

    protected $fillable = ["name", "rif", "status","phone","email","address_id","pricelist_id"];

    protected $dates = [];
    public $timestamps = false;

    public static $rules = [
        'name' => 'required|max:100',
        'rif' => 'required|max:12',
        'phone' => 'required|max:15',
        'email' => 'unique:App\Tercero,email|required',

    ];

    public static $messages = [
        'name.required' => 'El Nombre es Requerido',
        'name.max' => 'El Maximo de Caracteres es de 100',
        'rif.required' => 'El Rif o Cedula son Requerido',
        'rif.max' => 'El Maximo de Caracteres es de 12',
        'phone.required' => 'El Telefono es Requerido',
        'name.max' => 'El Maximo de Caracteres es de 15',
        'email.required' => 'El Correo es Requerido',
        'email.unique' => 'Este email se encuentra en la Base de Datos',

    ];

 //funciones Relaciones entre Tablas ----------------------------------------------------

    public function direccion()
        {
            return $this->hasMany('App\Address');
        }


    public function cobro()
    {
        return $this->hasMany('App\Cobro');
    }

    public function pedido()
    {
        return $this->hasMany('App\Pedido');
    }
    public function pricelist()
        {
            return $this->belongsTo('App\Pricelist');
        }


}
