<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricelist extends Model {
    protected $table = 'pricelist';
    protected $fillable = ["pricelistdetail_id","descripcion" ,"venta", "status"];

    protected $dates = [];
    public $timestamps = false;




    //funciones Relaciones entre Tablas ----------------------------------------------------

    public function producto()
        {
            return $this->hasMany('App\Producto');
        }
      
        public function pricelistdetail()
    {
        return $this->belongsTo('App\Pricelistdetail');
    }

    public function tercero()
        {
            return $this->hasMany('App\Tercero');
        }

}
