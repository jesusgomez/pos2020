<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cierre extends Model {

    protected $fillable = ["terminal_id", "sesion_id","type","status", "fecha"];



 // funcion Relacciones ---------------------------------------------------------------------------------------

    public function terminal()
        {
            return $this->belongsTo('App\Terminal');
        }

        public function sesion()
        {
            return $this->belongsTo('App\Sesion');
        }

        public function cierreCobroId()
        {
            return $this->hasMany('App\CierreCobro');
        }


}
