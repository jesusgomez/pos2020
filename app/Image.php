<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {

    protected $fillable = ["name", "url","status"];

    protected $dates = [];
    public $timestamps = false;

    public static $rules = [
        'name'=> 'required|max:30',
        'url'=> 'required|max:60',



    ];

    public static $messages = [
        'name.required' => 'El nombre es Requerido',
        'name.max' => 'El Maximo de Caracter es de 30',
        'url.required' => 'La Url es Requerida',
        'url.max' => 'El Maximo de Caracter es de 60',

    ];

 // funciones Relacion -------------------------------------------------------------------------------
        public function producto()
        {
            return $this->hasMany('App\Producto');
        }

}
