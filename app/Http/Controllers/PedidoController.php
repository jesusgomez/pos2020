<?php

namespace App\Http\Controllers;

use App\Pedido;
use Illuminate\Http\Request;
use Exception;
use Mockery\Undefined;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PedidoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }    
//--------------------------------------------------------------
 /* public function getTerceroRif($rif){
   
  try{
    $tercero = Pedido::where('rif',$rif)->firstOrFail();
    $tercero->direccion;
    $tercero->pricelist;
    $resul['tercero']= $tercero;
    //$resul['direccion']=$tercero->direccion;
    //$resul['priceList']=$tercero->pricelist->pricelistdetail;
    return $resul;
  }catch(ModelNotFoundException   $e){

  return 'no encontrado';

  }


}*/
 // -----------crear pedido --------------------------------------------------------
  public function crearPedido(Request $request) {
    $pedido = $request->input('pedido');
           
       
    /// llenado base datos pedido------------------------
        $dbPedido= new Pedido;
        $dbPedido['pedido_id']= $pedido['pedido_id'];
        $dbPedido['sesion_id']= $pedido['sesion_id'];
        $dbPedido['tercero_id']= $pedido['tercero_id'];
        $dbPedido['total']= $pedido['total'];
        $dbPedido['discount'] = $pedido['discount'];
        $dbPedido['tax'] = $pedido['tax'];
        $dbPedido['date'] = $pedido['date'];
        $dbPedido['type'] = $pedido['type'];
        $dbPedido['created_by'] = $pedido['created_by'];
        $dbPedido['updated_by'] = $pedido['updated_by'];
        $dbPedido['docstatus'] = $pedido['docstatus'];
        $dbPedido['issync'] = $pedido['issync'];
        $dbPedido['status'] = 'Y';
        $dbPedido-> save();   
       return $dbPedido;
  }
  // ---------------------------------------------------------------------------
   public function modificarPedido(Request $request) {
     $pedido = $request->input('pedido');
     
     // busqueda de Pedido------------------
      try {
        $dbPedidos = Pedido::where('id', $pedido['id'])->firstOrFail();
                
        $dbPedidos['type']= $pedido['type'];
        $dbPedidos['docstatus'] = $pedido['docstatus'];
        
        $dbPedidos-> update();  

      } catch (ModelNotFoundException $e ) {
        return 'Ese Numero de Pedido No se encuentra Registrado';
      }
   }
 //---------------------------------------------------------------------------
 public function eliminarPedido(Request $request) {
  $pedido = $request->input('pedido');
  
  // busqueda de tercero------------------
   try {
     $dbPedido = Pedido::where('id', $pedido['id'])->firstOrFail();
     
        $dbPedido['status'] = 'N';
        $dbPedido-> update();   

   } catch (ModelNotFoundException $e ) {
     return 'El Numero de Pedido no se encuentra registrado';
   }
}
//-----------------------------------------------------------------------------


    //
}
