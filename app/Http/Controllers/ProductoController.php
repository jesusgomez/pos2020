<?php

namespace App\Http\Controllers;
use App\Producto;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    
//--------------------------------------------------------------

  public function getProducto($prod){

  try{
    $producto = Producto::where('name',$prod)->firstOrFail();
    return $producto;
  }catch(NotFoundHttpException   $e){

  return 'no encontrado';

  }


}
 // -----------crear Producto --------------------------------------------------------
  public function crearProducto(Request $request) {
    $producto = $request->input('producto');
    
    //llenado base de datos Producto-----------------------
    try {
      $dbProducto = Producto::where('name', $producto['name'])->firstOrFail();
       return 'La Producto ya se encuentra almacenada';
    } catch (ModelNotFoundException $e) {
          
    /// llenado base datos Categoria------------------------
        $dbProducto= new Producto;
        $dbProducto['name']= $producto['name'];
        $dbProducto['codigo']= $producto['codigo'];
        $dbProducto['description']= $producto['description'];
        $dbProducto['uom']= $producto['uom'];
        $dbProducto['qty']= $producto['qty'];
        $dbProducto['isrecipe']= $producto['isrecipe'];
        $dbProducto['image_id']= $producto['image_id'];
        $dbProducto['product_id']= $producto['product_id'];
        $dbProducto['status'] = 'Y';
        $dbProducto-> save();
      }      
  }
  // ---------------------------------------------------------------------------
   public function modificarProducto(Request $request) {
     $producto = $request->input('producto');
        // busqueda de Usuario------------------
      try {
        $dbProducto = Producto::where('name', $producto['name'])->firstOrFail();
        $dbProducto['name']= $producto['name'];
        $dbProducto['codigo']= $producto['codigo'];
        $dbProducto['description']= $producto['description'];
        $dbProducto['uom']= $producto['uom'];
        $dbProducto['qty']= $producto['qty'];
        $dbProducto['isrecipe']= $producto['isrecipe'];
        $dbProducto['image_id']= $producto['image_id'];
        $dbProducto['product_id']= $producto['product_id'];
        $dbProducto-> update();  

      } catch (ModelNotFoundException $e ) {
        return 'La Producto no se encuentra registrado';
      }
   }
 //---------------------------------------------------------------------------
 public function eliminarProducto(Request $request) {
  $producto = $request->input('producto');
  
  // busqueda de Producto------------------
   try {
     $dbProducto = Producto::where('name', $producto['name'])->firstOrFail();
     
        $dbProducto['status'] = 'N';
        $dbProducto-> update();

     

   } catch (ModelNotFoundException $e ) {
     return 'La Producto no se encuentra registrado';
   }
}
//-----------------------------------------------------------------------------


    //
}
