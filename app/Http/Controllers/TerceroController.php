<?php

namespace App\Http\Controllers;

use App\Tercero;
use App\Address;
use Illuminate\Http\Request;
use Exception;
use Mockery\Undefined;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TerceroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getTercero($type){

      $tercero = Tercero::get()->first();
   if($type == Undefined)
     return $tercero->direccion;
     else
      return $tercero->direccion->where('type',$type);

    }

//--------------------------------------------------------------

  public function getTerceroRif($rif){
   
  try{
    $tercero = Tercero::where('rif',$rif)->firstOrFail();
    $tercero->direccion;
    $tercero->pricelist;
    $resul['tercero']= $tercero;
    //$resul['direccion']=$tercero->direccion;
    //$resul['priceList']=$tercero->pricelist->pricelistdetail;
    return $resul;
  }catch(ModelNotFoundException   $e){

  return 'no encontrado';

  }


}
//--------------------------------------------------------------
/*
  public function getTerceroAll(){
   
  
    $tercero = Tercero::()->get();
    $tercero->direccion;
    $tercero->pricelist;
    $resul['tercero']= $tercero;
    
    return $resul;
 


}*/
 // -----------crear tercero --------------------------------------------------------
  public function crearTerceros(Request $request) {
    $tercero = $request->input('tercero');
    $idPricelist = $request->input('priceList_id');
    $address = $request->input('address');
    
    //llenado base de datos direccion-----------------------
    
    try {
      $dbTerceros = Tercero::where('rif', $tercero['rif'])->firstOrFail();
       return 'El Tercero ya se encuentra';
    } catch (ModelNotFoundException $e) {
       
    /// llenado base datos terceros------------------------
        $dbTerceros= new Tercero;
        $dbTerceros['name']= $tercero['name'];
        $dbTerceros['rif']= $tercero['rif'];
        $dbTerceros['phone']= $tercero['phone'];
        $dbTerceros['email']= $tercero['email'];
        $dbTerceros['priceList_id'] = $idPricelist['id'];
        $dbTerceros['status'] = 'Y';
        $dbTerceros['address_id'] = 0; 
        $dbTerceros-> save();

    //llenado base de datos direccion-----------------------
        foreach ($address as $addres) {

          $dbAddress = new Address;
          $dbAddress['tercero_id'] = $dbTerceros['id'];
          $dbAddress['name'] = "";
          $dbAddress['address'] = $addres['address'];
          $dbAddress['type'] = $addres['type'];
          $dbAddress['status'] = 'Y';
          $dbAddress['phone'] = $addres['phone'];
          $dbAddress->save();
          
          if ($addres['cond'] == 1 ){
              $dbTerceros['address_id'] = $dbAddress['id'];
              $dbTerceros->update();

          }

        }
      }   
  }
  // ---------------------------------------------------------------------------
   public function modificarTerceros(Request $request) {
     $tercero = $request->input('tercero');
     $address = $request->input('address');
     $idPricelist = $request->input('priceList_id');
     // busqueda de tercero------------------
      try {
        $dbTerceros = Tercero::where('rif', $tercero['rif'])->firstOrFail();
        
        $dbTerceros['name']= $tercero['name'];
        $dbTerceros['rif']= $tercero['rif'];
        $dbTerceros['phone']= $tercero['phone'];
        $dbTerceros['email']= $tercero['email'];
        $dbTerceros['priceList_id'] = $idPricelist['id'];
        //$dbTerceros['address_id'] = 0; 
        $dbTerceros-> update();  

      } catch (ModelNotFoundException $e ) {
        return 'El Tercero no se encuentra registrado';
      }
   }
 //---------------------------------------------------------------------------
 public function eliminarTerceros(Request $request) {
  $tercero = $request->input('tercero');
  
  // busqueda de tercero------------------
   try {
     $dbTerceros = Tercero::where('rif', $tercero['rif'])->firstOrFail();
     
        $dbTerceros['status'] = 'N';
        $dbTerceros-> update();

     

   } catch (ModelNotFoundException $e ) {
     return 'El Tercero no se encuentra registrado';
   }
}
//-----------------------------------------------------------------------------


    //
}
