<?php

namespace App\Http\Controllers;
use App\Moneda;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    
//--------------------------------------------------------------

  public function getMoneda($mone){

  try{
    $moneda = Moneda::where('rif',$mone)->firstOrFail();
    return $moneda;
  }catch(NotFoundHttpException   $e){

  return 'no encontrado';

  }


}
 // -----------crear Moneda --------------------------------------------------------
  public function crearMoneda(Request $request) {
    $moneda = $request->input('moneda');
    
    //llenado base de datos Moneda-----------------------
    try {
      $dbMoneda = Moneda::where('name', $moneda['name'])->firstOrFail();
       return 'La Moneda ya se encuentra almacenada';
    } catch (ModelNotFoundException $e) {
          
    /// llenado base datos Categoria------------------------
        $dbMoneda= new Moneda;
        $dbMoneda['name']= $moneda['name'];
        $dbMoneda['symbol']= $moneda['symbol'];
        $dbMoneda['status'] = 'Y';
        $dbMoneda-> save();
      }      
  }
  // ---------------------------------------------------------------------------
   public function modificarMoneda(Request $request) {
     $moneda = $request->input('moneda');
        // busqueda de Usuario------------------
      try {
        $dbMoneda = Moneda::where('name', $moneda['name'])->firstOrFail();
        $dbMoneda['name']= $moneda['name'];
        $dbMoneda['symbol']= $moneda['symbol'];
        $dbMoneda-> update();  

      } catch (ModelNotFoundException $e ) {
        return 'La Moneda no se encuentra registrado';
      }
   }
 //---------------------------------------------------------------------------
 public function eliminarMoneda(Request $request) {
  $moneda = $request->input('moneda');
  
  // busqueda de Moneda------------------
   try {
     $dbMoneda = Moneda::where('name', $moneda['name'])->firstOrFail();
     
        $dbMoneda['status'] = 'N';
        $dbMoneda-> update();

     

   } catch (ModelNotFoundException $e ) {
     return 'La Moneda no se encuentra registrado';
   }
}
//-----------------------------------------------------------------------------


    //
}
