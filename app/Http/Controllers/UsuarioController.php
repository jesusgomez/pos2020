<?php

namespace App\Http\Controllers;


use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


//--------------------------------------------------------------

  public function getUsuario($user){

  try{
    $usuario = Usuario::where('rif',$user)->firstOrFail();
    return $usuario;
  }catch(NotFoundHttpException   $e){

  return 'no encontrado';

  }


}
//---------------------------------------------------------------------------
/*public function getUsuarioAll() {

}*/
// -----------crear usuario --------------------------------------------------------
  public function crearUsuario(Request $request) {
    $usuario = $request->input('usuario');

    //llenado base de datos usuario-----------------------
    try {
      $dbUsuario = Usuario::where('rif', $usuario['rif'])->firstOrFail();
       return 'El Usuario ya se encuentra';
    } catch (ModelNotFoundException $e) {

    /// llenado base datos Usuarios------------------------
        $dbUsuario= new Usuario;
        $dbUsuario['name']= $usuario['name'];
        $dbUsuario['user']= $usuario['user'];
        $dbUsuario['pass']= $usuario['pass'];
        $dbUsuario['status'] = 'Y';
        $dbUsuario-> save();
      }
  }
  // ---------------------------------------------------------------------------
   public function modificarUsuario(Request $request) {
     $usuario = $request->input('usuario');
        // busqueda de Usuario------------------
      try {
        $dbUsuario = Usuario::where('rif', $usuario['rif'])->firstOrFail();
        $dbUsuario['name']= $usuario['name'];
        $dbUsuario['pass']= $usuario['pass'];
        $dbUsuario-> update();

      } catch (ModelNotFoundException $e ) {
        return 'El Usuario no se encuentra registrado';
      }
   }
 //---------------------------------------------------------------------------
 public function eliminarUsuario(Request $request) {
  $usuario = $request->input('usuario');

  // busqueda de usuario------------------
   try {
     $dbUsuarios = Usuario::where('rif', $usuario['user'])->firstOrFail();

        $dbUsuarios['status'] = 'N';
        $dbUsuarios-> update();



   } catch (ModelNotFoundException $e ) {
     return 'El Usuario no se encuentra registrado';
   }
}
//-----------------------------------------------------------------------------


    //
}
