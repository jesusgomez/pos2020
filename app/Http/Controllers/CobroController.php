<?php

namespace App\Http\Controllers;
use App\Cobro;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CobroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
//--------------------------------------------------------------
/*
  public function getImage($ima){

  try{
    $image = Image::where('name',$ima)->firstOrFail();
    return $image;
  }catch(NotFoundHttpException   $e){

  return 'no encontrado';

  }

    
}*/
  //-------------------------------------------------------------------------------
  public function crearCobro(Request $request){
    $cobro = $request->input('cobro');
    /// llenado base datos Cobro------------------------
    
      $date = Carbon::now()->toDateTimeString();
      $dbCobro= new Cobro;
      $dbCobro['tercero_id']= $cobro['tercero_id'];
      $dbCobro['pedido_id']= $cobro['pedido_id'];
      $dbCobro['metodo_id']= $cobro['metodo_id'];
      $dbCobro['total']= $cobro['total'];
     $dbCobro['balance']= $cobro['balance'];
     // $dbCobro['created_at']= $date;
      //$dbCobro['updated_at']= $date;
      $dbCobro['created_by']= $cobro['created_by']; 
      $dbCobro['updated_by']= $cobro['updated_by']; 
      $dbCobro['status'] = 'Y';
      $dbCobro->save();
      return $dbCobro;
  }
 // -----------crear Cobro --------------------------------------------------------
  
  
//-----------------------------------------------------------------------------
public function buscarCobroXfecha($fecha){
  
    $dbCobro = Cobro::where('created_at', 'like', '%'.$fecha.'%')->get();
  return $dbCobro;
}
//-----------------------------------------------------------------------------
public function buscarCobroXtercero($tercero_id){
  
  
  $dbCobro = Cobro::where('tercero_id', $tercero_id)->get();
   return $dbCobro;
}

    //
}
