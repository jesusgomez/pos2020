<?php

namespace App\Http\Controllers;

use App\Sesion;
use Illuminate\Http\Request;
use Exception;
use Mockery\Undefined;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SesionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }    
//--------------------------------------------------------------
 /* public function getPricelist($rif){
   
  try{
    $tercero = Pedido::where('rif',$rif)->firstOrFail();
    $tercero->direccion;
    $tercero->pricelist;
    $resul['tercero']= $tercero;
    //$resul['direccion']=$tercero->direccion;
    //$resul['priceList']=$tercero->pricelist->Sesion;
    return $resul;
  }catch(ModelNotFoundException   $e){

  return 'no encontrado';

  }


}*/
 // -----------crear Sesion --------------------------------------------------------
  public function crearSesion(Request $request) {
    $sesion = $request->input('Sesion');
           
       
    /// llenado base datos Pricelist------------------------
        $dbSesion= new Sesion;
        $dbSesion['usuario_id']= $sesion['usuario_id'];
        $dbSesion['terminal_id']= $sesion['terminal_id'];
        $dbSesion['status'] = 'Y';
        $dbSesion-> save();   
       return $dbSesion;
  }
  
 //---------------------------------------------------------------------------
 public function eliminarSesion(Request $request) {
  $sesion = $request->input('sesion');
  
  // busqueda de tercero------------------
   try {
     $dbSesion = Sesion::where('id', $sesion['id'])->firstOrFail();
     
        $dbSesion['status'] = 'N';
        $dbSesion-> update();   

   } catch (ModelNotFoundException $e ) {
     return 'Sesion no Encontrada';
   }
}
//-----------------------------------------------------------------------------


    //
}
