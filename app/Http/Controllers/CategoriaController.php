<?php

namespace App\Http\Controllers;
use App\Categoria;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    
//--------------------------------------------------------------

  public function getCategoria($cate){

  try{
    $categoria = Categoria::where('rif',$cate)->firstOrFail();
    return $categoria;
  }catch(NotFoundHttpException   $e){

  return 'no encontrado';

  }


}
 // -----------crear categoria --------------------------------------------------------
  public function crearCategoria(Request $request) {
    $categoria = $request->input('categoria');
    
    //llenado base de datos categoria-----------------------
    try {
      $dbCategoria = Categoria::where('name', $categoria['name'])->firstOrFail();
       return 'La Categoria ya se encuentra almacenada';
    } catch (ModelNotFoundException $e) {
          
    /// llenado base datos Categoria------------------------
        $dbCategoria= new Categoria;
        $dbCategoria['name']= $categoria['name'];
        $dbCategoria['image']= $categoria['user'];
        $dbCategoria['description']= $categoria['description'];
        $dbCategoria['status'] = 'Y';
        $dbCategoria-> save();
      }      
  }
  // ---------------------------------------------------------------------------
   public function modificarCategoria(Request $request) {
     $categoria = $request->input('categoria');
        // busqueda de Usuario------------------
      try {
        $dbCategoria = Categoria::where('name', $categoria['name'])->firstOrFail();
        $dbCategoria['name']= $categoria['name'];
        $dbCategoria['description']= $categoria['description'];
        $dbCategoria-> update();  

      } catch (ModelNotFoundException $e ) {
        return 'La categoria no se encuentra registrado';
      }
   }
 //---------------------------------------------------------------------------
 public function eliminarCategoria(Request $request) {
  $categoria = $request->input('categoria');
  
  // busqueda de categoria------------------
   try {
     $dbCategoria = Categoria::where('name', $categoria['name'])->firstOrFail();
     
        $dbCategoria['status'] = 'N';
        $dbCategoria-> update();

     

   } catch (ModelNotFoundException $e ) {
     return 'La Categoria no se encuentra registrado';
   }
}
//-----------------------------------------------------------------------------


    //
}
