<?php

namespace App\Http\Controllers;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
//--------------------------------------------------------------

  public function getImage($ima){

  try{
    $image = Image::where('name',$ima)->firstOrFail();
    return $image;
  }catch(NotFoundHttpException   $e){

  return 'no encontrado';

  }


}
 // -----------crear Image --------------------------------------------------------
  public function crearImage(Request $request) {
    $image = $request->input('image');
    
    //llenado base de datos Image-----------------------
    try {
      $dbImage = Image::where('name', $image['name'])->firstOrFail();
       return 'La Image ya se encuentra almacenada';
    } catch (ModelNotFoundException $e) {
          
    /// llenado base datos Categoria------------------------
        $dbImage= new Image;
        $dbImage['name']= $image['name'];
        $dbImage['url']= $image['url'];
        $dbImage['status'] = 'Y';
        $dbImage-> save();
      }      
  }
  // ---------------------------------------------------------------------------
   public function modificarImage(Request $request) {
     $image = $request->input('image');
        // busqueda de Usuario------------------
      try {
        $dbImage = Image::where('name', $image['name'])->firstOrFail();
        $dbImage['name']= $image['name'];
        $dbImage['url']= $image['url'];
        $dbImage-> update();  

      } catch (ModelNotFoundException $e ) {
        return 'La Imagen no se encuentra registrado';
      }
   }
 //---------------------------------------------------------------------------
 public function eliminarImage(Request $request) {
  $image = $request->input('image');
  
  // busqueda de Image------------------
   try {
     $dbImage = Image::where('name', $image['name'])->firstOrFail();
        $dbImage['status'] = 'N';
        $dbImage-> update();
    

   } catch (ModelNotFoundException $e ) {
     return 'La Image no se encuentra registrado';
   }
}
//-----------------------------------------------------------------------------


    //
}
