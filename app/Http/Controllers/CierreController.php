<?php

namespace App\Http\Controllers;
use App\Cierre;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CierreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
//--------------------------------------------------------------
/*
  public function getImage($ima){

  try{
    $image = Image::where('name',$ima)->firstOrFail();
    return $image;
  }catch(NotFoundHttpException   $e){

  return 'no encontrado';

  }


}*/
  //-------------------------------------------------------------------------------
  public function cierre($cierre){
      /// llenado base datos Cierre------------------------
      $date = Carbon::now()->toDateTimeString();
      $dbCierre= new Cierre;
      $dbCierre['terminal_id']= $cierre['terminal_id'];
      $dbCierre['sesion_id']= $cierre['sesion_id'];
      $dbCierre['type']= $cierre['type'];
      $dbCierre['fecha']= $date;        
      $dbCierre['status'] = 'Y';
      $dbCierre-> save();
      //return $onlyDate[0];
  }
 // -----------crear Cierre --------------------------------------------------------
  public function crearCierreZ(Request $request) {
    $cierre = $request->input('cierre');
    
    $date = Carbon::now()->toDateTimeString();
    $onlyDate = explode(' ', $date);
    //llenado base de datos Image-----------------------
    
    try {
      $dbCierre = Cierre::where('type', 'Z')->where('fecha', $onlyDate[0])->firstOrFail();
       return 'Ya Realizo un cierre Z el dia de hoy';
    } catch (ModelNotFoundException $e) {
          
      return $this->cierre($cierre);
      }
        
  }
  
  // ---------------------------------------------------------------------------
  public function crearCierre(Request $request) {
    $cierre = $request->input('cierre');
   
      if ($cierre['type'] == 'Z'){
        return 'Ya Realizo un cierre Z el dia de hoy';
      }
        else
        {
      return $this->cierre($cierre);
      }
  }
//-----------------------------------------------------------------------------
public function buscarCierre($fecha){
  
    $dbCierre = Cierre::where('fecha', $fecha)->get();
  return $dbCierre;
}
//-----------------------------------------------------------------------------


    //
}
