<?php

namespace App\Http\Controllers;
use App\Conversion;
use Illuminate\Http\Request;


class ConversionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

  //-------------------------------------------------------------------------------
  public function crearConversion(Request $request){
    $conversion = $request->input('conversion');
    /// llenado base datos Convercion------------------------
    
    
      //$date = Carbon::now()->toDateTimeString();
      $dbConversion= new Conversion;
      $dbConversion['moneda_id']= $conversion['moneda_id'];
      $dbConversion['factor']= $conversion['factor'];
      $dbConversion['created_by']= $conversion['created_by'];
      $dbConversion['updated_by']= $conversion['updated_by'];
      $dbConversion['status'] = 'Y';
      $dbConversion->save();
      return $dbConversion;
  }
 // -----------crear Cobro --------------------------------------------------------
  

//-----------------------------------------------------------------------------
public function buscarConversionXfecha($fecha){
  
    $dbConversion = Conversion::where('created_at', 'like', '%'.$fecha.'%')->get();
  return $dbConversion;
}


    //
}
