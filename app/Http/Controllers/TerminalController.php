<?php

namespace App\Http\Controllers;

use App\Terminal;
use Illuminate\Http\Request;
use Exception;
use Mockery\Undefined;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TerminalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }    
//--------------------------------------------------------------
 /* public function getPricelist($rif){
   
  try{
    $tercero = Pedido::where('rif',$rif)->firstOrFail();
    $tercero->direccion;
    $tercero->pricelist;
    $resul['tercero']= $tercero;
    //$resul['direccion']=$tercero->direccion;
    //$resul['priceList']=$tercero->pricelist->Terminal;
    return $resul;
  }catch(ModelNotFoundException   $e){

  return 'no encontrado';

  }


}*/
 // -----------crear Terminal --------------------------------------------------------
  public function crearTerminal(Request $request) {
    $terminal = $request->input('terminal');
           
       
    /// llenado base datos Pricelist------------------------
        $dbTerminal= new Terminal;
        $dbTerminal['name']= $terminal['name'];
        $dbTerminal['ip']= $terminal['ip'];
        $dbTerminal['mac']= $terminal['mac'];
        $dbTerminal['port']= $terminal['port'];
        $dbTerminal['type']= $terminal['type'];
        $dbTerminal['terminal_id']= $terminal['terminal_id'];
        $dbTerminal['status'] = 'Y';
        $dbTerminal-> save();   
       return $dbTerminal;
  }
  // ---------------------------------------------------------------------------
   public function modificarTerminal(Request $request) {
     $terminal = $request->input('terminal');
     
     // busqueda de pricelist------------------
      try {
        $dbTerminal = Terminal::where('id', $terminal['id'])->firstOrFail();
        $dbTerminal['name']= $terminal['name'];
        $dbTerminal['ip']= $terminal['ip'];
        $dbTerminal['mac']= $terminal['mac'];
        $dbTerminal['port']= $terminal['port'];
        $dbTerminal['type']= $terminal['type'];
        $dbTerminal['terminal_id']= $terminal['terminal_id'];
        $dbTerminal-> update();  

      } catch (ModelNotFoundException $e ) {
        return 'Terminal no encontrada';
      }
   }
 //---------------------------------------------------------------------------
 public function eliminarTerminal(Request $request) {
  $terminal = $request->input('terminal');
  
  // busqueda de tercero------------------
   try {
     $dbTerminal = Terminal::where('id', $terminal['id'])->firstOrFail();
     
        $dbTerminal['status'] = 'N';
        $dbTerminal-> update();   

   } catch (ModelNotFoundException $e ) {
     return 'Terminal no encontrada';
   }
}
//-----------------------------------------------------------------------------
  
}
