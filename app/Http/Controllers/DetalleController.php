<?php

namespace App\Http\Controllers;
use App\Detalle;
use Illuminate\Http\Request;


class DetalleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

  //-------------------------------------------------------------------------------
  public function crearDetalle(Request $request){
    $detalle = $request->input('detalle');
    /// llenado base datos Convercion------------------------
    
    
      //$date = Carbon::now()->toDateTimeString();
      $dbDetalle= new Detalle;
      $dbDetalle['pedido_id']= $detalle['pedido_id'];
      $dbDetalle['producto_id']= $detalle['producto_id'];
      $dbDetalle['name']= $detalle['name'];
      $dbDetalle['qty']= $detalle['qty'];
      $dbDetalle['price']= $detalle['price'];
      $dbDetalle['discount']= $detalle['discount'];
      $dbDetalle['tax']= $detalle['tax'];
      $dbDetalle['status'] = 'Y';
      $dbDetalle->save();
      return $dbDetalle;
  }
 // -----------crear Cobro --------------------------------------------------------
  

//-----------------------------------------------------------------------------
public function buscarDetalleXfecha($fecha){
  
    $dbDetalle = Detalle::where('created_at', 'like', '%'.$fecha.'%')->get();
  return $dbDetalle;
}


    //
}
