<?php

namespace App\Http\Controllers;

use App\Tercero;
use App\Address;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AddressController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAddressTercero($terceroId){
      try{ 
      $address = Address::where('tercero_id', $terceroId)->get();
      return $address;
      }catch(NotFoundHttpException $e){
         return 'direccion no encontrada o tercero no registrado';
      }
   
    }

   //--------------------------------------------------------------
   public function modificarAddress(Request $request){
     
     $address = $request->input('address');
      
     try {
        $dbAddress = Address::find($address['id']);
          $dbAddress['name'] = null;
          $dbAddress['address'] = $address['address'];
          $dbAddress['type'] = $address['type'];
          $dbAddress['status'] = $address['type'];;
          $dbAddress['phone'] = $address['phone'];
          $dbAddress->update();
          return 'Modificacion Exitosa';
     } catch(ModelNotFoundException $e) {

     }
      
   }
  //----------------------------------------------------------------
  public function eliminarAddress(Request $request){
    $address = $request->input('address');
      
     try {
        $dbAddress = Address::find($address['id']);
        $dbAddress['status'] = $address['type'];;
          $dbAddress->update();
        return 'direccion Eliminada Sactifactoriamente';
     }catch(ModelNotFoundException $e) {

     }

  }
  //------------------------------------------------
    //
}
