<?php

namespace App\Http\Controllers;

use App\Pricelistdetail;
use Illuminate\Http\Request;
use Exception;
use Mockery\Undefined;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PricelistdetailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }    
//--------------------------------------------------------------
 /* public function getPricelist($rif){
   
  try{
    $tercero = Pedido::where('rif',$rif)->firstOrFail();
    $tercero->direccion;
    $tercero->pricelist;
    $resul['tercero']= $tercero;
    //$resul['direccion']=$tercero->direccion;
    //$resul['priceList']=$tercero->pricelist->pricelistdetail;
    return $resul;
  }catch(ModelNotFoundException   $e){

  return 'no encontrado';

  }


}*/
 // -----------crear Pricelistdetail --------------------------------------------------------
  public function crearPricelistdetail(Request $request) {
    $pricelistdetail = $request->input('pricelistdetail');
           
       
    /// llenado base datos Pricelist------------------------
        $dbPricelistdetail= new Pricelistdetail;
        $dbPricelistdetail['product_id']= $pricelistdetail['product_id'];
        $dbPricelistdetail['price']= $pricelistdetail['price'];
        $dbPricelistdetail['pricelist_id']= $pricelistdetail['pricelist_id'];
        $dbPricelistdetail['status'] = 'Y';
        $dbPricelistdetail-> save();   
       return $dbPricelistdetail;
  }
  // ---------------------------------------------------------------------------
   public function modificarPricelistdetail(Request $request) {
     $pricelistdetail = $request->input('pricelistdetail');
     
     // busqueda de pricelist------------------
      try {
        $dbPricelistdetail = pricelistdetail::where('id', $pricelistdetail['id'])->firstOrFail();
                
        $dbPricelistdetail['price']= $pricelistdetail['price'];         
        $dbPricelistdetail-> update();  

      } catch (ModelNotFoundException $e ) {
        return 'se modifico exitosamente la lista';
      }
   }
 //---------------------------------------------------------------------------
 public function eliminarPricelistdetail(Request $request) {
  $pricelistdetail = $request->input('pricelistdetail');
  
  // busqueda de tercero------------------
   try {
     $dbPricelistdetail = pricelistdetail::where('id', $pricelistdetail['id'])->firstOrFail();
     
        $dbPricelistdetail['status'] = 'N';
        $dbPricelistdetail-> update();   

   } catch (ModelNotFoundException $e ) {
     return 'la lista de precio fue eliminada';
   }
}
//-----------------------------------------------------------------------------
  
}
