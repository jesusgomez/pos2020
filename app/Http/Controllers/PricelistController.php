<?php

namespace App\Http\Controllers;

use App\Pricelist;
use Illuminate\Http\Request;
use Exception;
use Mockery\Undefined;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PricelistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }    
//--------------------------------------------------------------
 /* public function getPricelist($rif){
   
  try{
    $tercero = Pedido::where('rif',$rif)->firstOrFail();
    $tercero->direccion;
    $tercero->pricelist;
    $resul['tercero']= $tercero;
    //$resul['direccion']=$tercero->direccion;
    //$resul['priceList']=$tercero->pricelist->pricelistdetail;
    return $resul;
  }catch(ModelNotFoundException   $e){

  return 'no encontrado';

  }


}*/
 // -----------crear pricelist --------------------------------------------------------
  public function crearPricelist(Request $request) {
    $Pricelist = $request->input('Pricelist');
           
       
    /// llenado base datos Pricelist------------------------
        $dbPricelist= new Pricelist;
        $dbPricelist['pricelistdetail_id']= $Pricelist['pricelistdetail_id'];
        $dbPricelist['descripcion']= $Pricelist['descripcion'];
        $dbPricelist['venta']= $Pricelist['venta'];
        $dbPricelist['status'] = 'Y';
        $dbPricelist-> save();   
       return $dbPricelist;
  }
  // ---------------------------------------------------------------------------
   public function modificarPricelist(Request $request) {
     $pricelist = $request->input('pricelist');
     
     // busqueda de pricelist------------------
      try {
        $dbPricelists = pricelist::where('id', $pricelist['id'])->firstOrFail();
                
        $dbPricelists['descripcion']= $pricelist['descripcion'];
        $dbPricelists['venta'] = $pricelist['venta'];
        
        $dbPricelists-> update();  

      } catch (ModelNotFoundException $e ) {
        return 'la lista de precio no se encuentra registrada';
      }
   }
 //---------------------------------------------------------------------------
 public function eliminarPricelist(Request $request) {
  $pricelist = $request->input('pricelist');
  
  // busqueda de tercero------------------
   try {
     $dbPricelist = Pricelist::where('id', $pricelist['id'])->firstOrFail();
     
        $dbPricelist['status'] = 'N';
        $dbPricelist-> update();   

   } catch (ModelNotFoundException $e ) {
     return 'la lista de precio fue eliminada';
   }
}
//-----------------------------------------------------------------------------


    //
}
