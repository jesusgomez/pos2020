<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model {

    protected $fillable = ["name", "descripcion","status"];

    protected $dates = [];
    public $timestamps = false;

    public static $rules = [
        'name' => 'required|max:40',
        'descripcion' => 'max:255|size:255',
      ];

    public static $messages = [
        'name.required' => 'El Nombre es Requerido',
        'name.max' => 'El Maximo de caracter es de 40',
        'descripcion.max' => 'El Maximo de caracter es de 255',
      ];


  // funciones Relacion -------------------------------------------------------
  public function producto_medida()
        {
            return $this->hasMany('App\ProductoMedida');
        }
}
