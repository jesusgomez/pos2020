<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model {

    protected $fillable = ["name", "image","description","status"];

    public $timestamps = false;

    public static $rules = [
        'name' => 'required|max:60|string|size:60',
        'description' => 'required|max:11|unique:posts|integer|size:11',

    ];

    public static $messages = [
        'name.required' => 'El nombre es Requerido',
        'name.max' => 'El maximo de Caracteres es de 60',
        'description.required'  => 'La Descripcion es Requerida',
        'description.max' => 'El maximo de Caracteres Numericos es de 11',
        'description.integer' => 'El Campo es solo Numerico',

    ];

    public function producto()
    {
        return $this->hasMany('App\Producto');
    }


}
