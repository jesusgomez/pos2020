<?php namespace App;
 
use Illuminate\Database\Eloquent\Model;

class CierreCobro extends Model {
    protected $table = 'cierre_cobro';
    protected $fillable = ["cierre_id", "cobro_id"];

    protected $dates = [];
    public $timestamps = false;

    public static $rules = [

    ];

    public static $messages = [
        
    ];

    // funcion Relacciones ---------------------------------------------------------------------------------------

    public function cobroAcierre()
        {
            return $this->belongsTo('App\Cierre');
        }

        public function cierreAcobro()
        {
            return $this->belongsTo('App\Cobro');
        }
    
}