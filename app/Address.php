<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {
    protected $table = 'address';
    protected $fillable = ["name", "address","type","status","phone","tercero_id"];

    protected $dates = [];
    public $timestamps = false;

    public static $rules = [
        'name'=> 'required|max:30',
        'address' => 'required|max:255',
        'phone' => 'required|max:15',
        'type' => 'in:Fiscal,Despacho,Domicilio,Otra',

    ];

    public static $messages = [
        'name.required' => 'El nombre es Requerido',
        'name.max' => 'El Maximo es de 30 Caracteres',
        'address.required'  => 'La Direccion es Requerida',
        'address.max' => 'El Maximo es de 255 Caracteres',
        'phone.required'  => 'El Telefono es Requerido',
        'phone.max' => 'El Maximo es de 15 Caracteres',
        'type.in' => 'Solo es aceptado los siguientes: "Fiscal,Despacho,Domicilio,Otra" ',


    ];
   // funcion Relacciones ---------------------------------------------------------------------------------------

 public function tercero()
    {
        return $this->belongsTo('App\Tercero');
    }


}
